﻿using NUnit.Framework;
using BusinessLogic;
using Moq;
using System.Collections.Generic;

namespace BusinessLogicTest
{
    [TestFixture]
    public class DivisibilityRule
    {
        IDivisibilityRule fizzBuzz;
        List<string> OutputList = new List<string>(new string[] { "1", "2", "Fizz", "4", "Buzz" });

        [SetUp]
        public void GeneralSetup()
        {
            var mockDivisible = new Mock<IGetMessages>();

            mockDivisible.Setup(x => x.IsDivisible(It.IsAny<int>())).Returns(true);
            mockDivisible.Setup(x => x.GetMessages(It.Is<int>(y => y == 1))).Returns("1");
            mockDivisible.Setup(x => x.GetMessages(It.Is<int>(y => y == 2))).Returns("2");
            mockDivisible.Setup(x => x.GetMessages(It.Is<int>(y => y == 3))).Returns("Fizz");
            mockDivisible.Setup(x => x.GetMessages(It.Is<int>(y => y == 4))).Returns("4");
            mockDivisible.Setup(x => x.GetMessages(It.Is<int>(y => y == 5))).Returns("Buzz");

            var list = new List<IGetMessages> { mockDivisible.Object };
            fizzBuzz = new GetMessages(list);
        }

        [Test]
        public void DivisibilityCheckOperationTests_DivisibilityCheckOperation_Operations_ShouldReturnListOfStrings()
        {
            var resultList = fizzBuzz.DivisibilityRule(5);

            //Assert
            Assert.AreEqual(OutputList, resultList);
        }

    }

}
