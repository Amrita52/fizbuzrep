﻿using NUnit.Framework;
using BusinessLogic;

namespace BusinessLogicTest
{
    [TestFixture]
    public class NonDivisibilityCheckTest
    {
        IGetMessages divisibleOperation;
        [SetUp]
        public void GeneralFunction()
        {
            divisibleOperation = new NonDivisibilityCheck();
        }

        [Test]
        public void NotDivisibleCheck_ShouldReturnNumber()
        {
            //Act
            var result = divisibleOperation.GetMessages(4);

            //Assert
            Assert.AreEqual(result, "4");
        }

    }

}
