﻿using System;
using NUnit.Framework;
using BusinessLogic;
using Moq;

namespace BusinessLogicTest
{
    [TestFixture]
    public class DivisibilityCheckByThreeAndFiveTest
    {
        IGetMessages divisibleOperation;
        Mock<ISpecifiedDayCheck> mock = new Mock<ISpecifiedDayCheck>();

        [SetUp]
        public void Setup()
        {
            divisibleOperation = new DivisibilityCheckByThreeAndFive(mock.Object);
        }

        [Test]
        public void DivisibilityCheckByThreeAndFiveTests_DivisibilityCheckByThreeAndFive_IsDivisible_ShouldReturnTrue()
        {
            //Act
            var result = divisibleOperation.IsDivisible(15);

            //Act
            Assert.AreEqual(result, true);
        }

        [Test]
        public void DivisibilityCheckByThreeAndFiveTests_DivisibilityCheckByThreeAndFive_IsDivisible_ShouldReturnFalse()
        {
            //Act
            var result = divisibleOperation.IsDivisible(9);

            //Act
            Assert.AreEqual(result, false);
        }

        [Test]
        public void DivisibilityCheckByThreeAndFiveTests_DivisibilityCheckByThreeAndFive_IsDivisible_ShouldReturnFalse_case2()
        {
            //Act
            var result = divisibleOperation.IsDivisible(5);

            //Act
            Assert.AreEqual(result, false);
        }

        [Test]
        public void DivisibilityCheckByThreeAndFiveTests_DivisibilityCheckByThreeAndFive_ShouldReturnWizzWuzz_WhenDivisibleByThreeAndFive_And_SpecifiedDayCheckIsTrue()
        {
            //Arrange
            mock.Setup(x => x.IsSpecificDayIsWednesday(DateTime.Now.DayOfWeek)).Returns(true);

            //Act
            var result = divisibleOperation.GetMessages(15);

            //Assert
            Assert.AreEqual(result, "WizzWuzz");
        }

        [Test]
        public void DivisibilityCheckByThreeAndFiveTests_DivisibilityCheckByThreeAndFive_ShouldReturnNull_WhenNotDivisibleByThreeAndFive_And_SpecifiedDayCheckIsTrue()
        {
            //Arrange
            mock.Setup(x => x.IsSpecificDayIsWednesday(DateTime.Now.DayOfWeek)).Returns(true);

            //Act
            var result = divisibleOperation.GetMessages(10);

            //Assert
            Assert.AreEqual(result, null);
        }

        [Test]
        public void DivisibilityCheckByThreeAndFiveTests_DivisibilityCheckByThreeAndFive_ShouldReturnFizzBuzz_WhenDivisibleByThreeAndFive_And_SpecifiedDayCheckIsFalse()
        {
            //Arrange
            mock.Setup(x => x.IsSpecificDayIsWednesday(DateTime.Now.DayOfWeek)).Returns(false);

            //Act
            var result = divisibleOperation.GetMessages(15);

            //Assert
            Assert.AreEqual(result, "Fizz Buzz");
        }

        [Test]
        public void DivisibilityCheckByThreeAndFiveTests_DivisibilityCheckByThreeAndFive_ShouldReturnNull_WhenNotDivisibleByThreeAndFive_And_SpecifiedDayCheckIsFalse()
        {
            //Arrange
            mock.Setup(x => x.IsSpecificDayIsWednesday(DateTime.Now.DayOfWeek)).Returns(false);

            //Act
            var result = divisibleOperation.GetMessages(9);

            //Assert
            Assert.AreEqual(result, null);
        }

    }

}
