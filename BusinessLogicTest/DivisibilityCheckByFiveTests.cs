﻿using System;
using NUnit.Framework;
using BusinessLogic;
using Moq;

namespace BusinessLogicTest
{
    public class DivisibilityCheckByFiveTests
    {
        IGetMessages divisibleOperation;
        Mock<ISpecifiedDayCheck> mock = new Mock<ISpecifiedDayCheck>();

        [SetUp]
        public void Setup()
        {
            divisibleOperation = new DivisibilityCheckByFive(mock.Object);

        }

        [Test]
        public void DivisibilityCheckByFiveTests_DivisibilityCheckByFive_IsDivisible_ShouldReturnTrue()
        {
            //Arrange
            var result = divisibleOperation.IsDivisible(10);

            //Act
            Assert.AreEqual(result, true);
        }

        [Test]
        public void DivisibilityCheckByFiveTests_DivisibilityCheckByFive_IsDivisible_ShouldReturnFalse()
        {
            //Arrange
            var result = divisibleOperation.IsDivisible(9);

            //Act
            Assert.AreEqual(result, false);
        }

        [Test]
        public void DivisibilityCheckByFiveTests_DivisibilityCheckByFive_IsDivisible_ShouldReturnFalse_()
        {
            //Arrange
            var result = divisibleOperation.IsDivisible(15);

            //Act
            Assert.AreEqual(result, false);
        }

        [Test]
        public void DivisibilityCheckByFiveTests_DivisibilityCheckByFive_ShouldReturnNull_WhenNotDivisibleByFive_And_SpecifiedDayCheckIsTrue()
        {
            //Arrange
            mock.Setup(x => x.IsSpecificDayIsWednesday(DateTime.Now.DayOfWeek)).Returns(true);

            //Act
            var result = divisibleOperation.GetMessages(9);

            //Assert
            Assert.AreEqual(result, null);
        }

        [Test]
        public void DivisibilityCheckByFiveTests_DivisibilityCheckByFive_ShouldReturnBuzz_WhenDivisibleByFive_And_SpecifiedDayCheckIsFalse()
        {
            //Arrange
            mock.Setup(x => x.IsSpecificDayIsWednesday(DateTime.Now.DayOfWeek)).Returns(false);

            //Act
            var result = divisibleOperation.GetMessages(10);

            //Assert
            Assert.AreEqual(result, "Buzz");
        }

        [Test]
        public void DivisibilityCheckByFiveTests_DivisibilityCheckByFive_ShouldReturnNull_WhenNotDivisibleByFive_And_SpecifiedDayCheckIsFalse()
        {
            //Arrange
            mock.Setup(x => x.IsSpecificDayIsWednesday(DateTime.Now.DayOfWeek)).Returns(false);

            //Act
            var result = divisibleOperation.GetMessages(9);

            //Assert
            Assert.AreEqual(result, null);
        }

        [Test]
        public void DivisibilityCheckByFiveTests_DivisibilityCheckByFive_ShouldReturnWuzz_WhenDivisibleByFive_And_SpecifiedDayCheckIsTrue()
        {
            //Arrange
            mock.Setup(x => x.IsSpecificDayIsWednesday(DateTime.Now.DayOfWeek)).Returns(true);

            //Act
            var result = divisibleOperation.GetMessages(10);

            //Assert
            Assert.AreEqual(result, "Wuzz");
        }

    }

}
