﻿using System;

namespace BusinessLogic
{
    public class DivisibilityCheckByThreeAndFive : IGetMessages
    {
        private ISpecifiedDayCheck isSpecifiedDay;

        public DivisibilityCheckByThreeAndFive(ISpecifiedDayCheck isSpecifiedDay)
        {
            this.isSpecifiedDay = isSpecifiedDay;
        }

        public bool IsDivisible(int number)
        {
            return number % 5 == 0 && number % 3 == 0 ? true : false;
        }

        public string GetMessages(int number)
        {
            if (IsDivisible(number))
            {
                return isSpecifiedDay.IsSpecificDayIsWednesday(DateTime.Now.DayOfWeek) ? "WizzWuzz" : "Fizz Buzz";
            }
            else
            {
                return null;
            }

        }

    }

}
