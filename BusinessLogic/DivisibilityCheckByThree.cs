﻿using System;

namespace BusinessLogic
{
    public class DivisibilityCheckByThree : IGetMessages
    {
        private ISpecifiedDayCheck isSpecifiedDay;

        public DivisibilityCheckByThree(ISpecifiedDayCheck isSpecifiedDay)
        {
            this.isSpecifiedDay = isSpecifiedDay;
        }

        public bool IsDivisible(int number)
        {
            return number % 3 == 0 && number % 5 != 0 ? true : false;
        }

        public string GetMessages(int number)
        {
            if (IsDivisible(number))
            {
                return isSpecifiedDay.IsSpecificDayIsWednesday(DateTime.Now.DayOfWeek) ? "Wizz" : "Fizz";
            }
            else
            {
                return null;
            }

        }

    }

}

    


