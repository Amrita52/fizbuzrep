﻿using System;

namespace BusinessLogic
{
    public class SpecifiedDayCheck : ISpecifiedDayCheck
    {
        public bool IsSpecificDayIsWednesday(DayOfWeek day)
        {
            return day == DayOfWeek.Wednesday;
        }
    }

}
