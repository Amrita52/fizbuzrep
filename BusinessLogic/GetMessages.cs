﻿using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic
{
    public class GetMessages : IDivisibilityRule
    {
        private IList<IGetMessages> divisibleOperations;

        public GetMessages(IList<IGetMessages> divisibleOperations)
        {
            this.divisibleOperations = divisibleOperations;
        }

        public IList<string> DivisibilityRule(int number)
        {
            var result = new List<string>();

            for (int index = 1; index <= number; index++)
            {
                var items = this.divisibleOperations.FirstOrDefault(x => x.IsDivisible(index));
                result.Add(items.GetMessages(index));
            }

            return result;
            
        }

    }

}



