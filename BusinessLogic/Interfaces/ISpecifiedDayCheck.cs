﻿using System;

namespace BusinessLogic
{
    public interface ISpecifiedDayCheck
    {
        bool IsSpecificDayIsWednesday(DayOfWeek day);
    }

}
