﻿using BusinessLogic;
using System.Web.Mvc;
using FizzBuzz.Models;
using PagedList;

namespace FizzBuzz.Controllers
{ 
    public class HomeController : Controller
    {
        private readonly IDivisibilityRule fizzBuzz;

        public HomeController(IDivisibilityRule fizzBuzz)
        {
            this.fizzBuzz = fizzBuzz;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View(new FizzBuzzParameters());
        }

        [HttpGet]
        public ActionResult DisplayResult(FizzBuzzParameters inputsFromUser, int? i)
        {
            if (ModelState.IsValid)
            {
                inputsFromUser.OutputList = this.fizzBuzz.DivisibilityRule(inputsFromUser.InputNumber).ToPagedList(i ?? 1, 20);
                return View("DisplayResult", inputsFromUser);
            }
            else
            {
                return View("Index", inputsFromUser);
            }

        }
      
    }

}